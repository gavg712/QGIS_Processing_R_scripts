##Raster processing=group
##RasterCover=name
##Replace raster values with a second raster=display_name 
##Target_raster=raster
##Replacement_raster=raster
##Target_value=number 0
##Fill_nulls=optional boolean False
##Covered=output raster

if(!Fill_nulls) 
  Target_raster[Target_raster == Target_value] <- NA
Covered <- cover(Target_raster, Replacement_raster)
