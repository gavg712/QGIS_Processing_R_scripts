##Vector processing=group
##Rasterize and aggregate=name
##Layer=vector
##Rasterize_field=field Layer
##Aggregate_fun=enum literal sum;mean;sd;min;max;median;count
##Pixel_size_x=number 1.0
##Pixel_size_y=number 1.0
##Extent=extent
##Result=output raster
count <- function(x, na.rm=T){if(na.rm) length(x[!is.na(x)]) else length(x)}
r <- raster(x = extent(Extent),
            resolution = c(Pixel_size_x, Pixel_size_y),
            crs = crs(Layer))
r[] <- 0
f <- function(i, Layer = Layer, Field = Rasterize_field, base = r){
  rasterize(Layer[i,], base, field = Field)}
rs <- lapply(seq_len(nrow(Layer)), f, Layer=Layer, 
             Field = Rasterize_field, base = r)
s <- stack(rs)
Result <- raster::calc(s, fun = match.fun(Aggregate_fun), na.rm = T)
