##Vector processing=group
##Nearest feature=name
##N-Nearest features table=display_name
##Layer=vector
##Id_field=Field Layer
##N_neighbohrs=number 1
##Which=optional enum Great_Circle;Euclidean;Hausdorff;Frechet
##Nearest_table=output table
n <- N_neighbohrs
if(n >= nrow(Layer))
    stop("\nNumber of neighbohrs exceeds number of features. Max must be k-1 features")

if(length(unique(Layer[[Id_field]])) != length(Layer[[Id_field]]))
    stop(sprintf("\nField '%s' contains non unique values. Please choose another one!\n", Id_field))


if(is.null(Which)){
    Which <- ifelse(isTRUE(st_is_longlat(Layer)),
                    "Great Circle",
                    "Euclidean")
} else {
    Which <- c("Great Circle", "Euclidean", 
               "Hausdorff", "Frechet")[Which + 1]}

if(isTRUE(st_is_longlat(Layer) | Which == "Great Circle")) {
    warning(sprintf("Distance method '%s' not valid for lonlat CRS. Changing to 'Great Circle'", Which))
    Which <- "Great Circle"
}

pdis <- st_distance(Layer, which = Which)
diag(pdis) <- NA
out <- data.frame(id = Layer[[Id_field]])
i = 1
while(i <= n){
    mins <- apply(pdis, 1, which.min)
    out[[paste0("near", i, "_id")]] <- Layer[[Id_field]][mins]
    out[[paste0("near", i, "_dist")]] <- apply(pdis, 1, min, na.rm = TRUE)
    for(r in seq_len(nrow(pdis))) pdis[r, mins[r]] <- NA
    i = i + 1
}
cat(sprintf("\nReady, table with %s nearest neighbohrs IDs and distances", n))
cat(sprintf("\nDistances calculated by '%s' method\n", Which))

Nearest_table <- out