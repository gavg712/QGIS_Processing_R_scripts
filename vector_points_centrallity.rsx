##Vector processing=group
##Points centrality=name
##Layer=vector point
##Spatial_center=enum literal all;mean.center;weighted.mean.center;median.center;central.feature all
##Weights_field=optional field Layer
##Centrality=output vector

xy <- st_coordinates(Layer)
Weights_field <- if(!is.null(Weights_field)) Layer[[Weights_field]]

# Functions
mean_mc <- function(w = NULL){
  if(is.null(w) && Spatial_center == "weighted.mean.center")
    warning("Weights field is null. Mean center instead!")
  
  if(!is.null(w)) {
    m <- apply(xy, 2, weighted.mean, w = w)
  } else {m <- apply(xy, 2, mean)}
  st_point(m)
  }

median_mc <- function() st_point(apply(xy, 2, median))

central_feature <- function(){
  d <- st_distance(Layer)
  d <- apply(d, 1, sum)
  st_point(xy[which.min(d),])
}

all_features <- function(){
  if(!is.null(Weights_field))
    st_sfc(mean_mc(), median_mc(), central_feature(), mean_mc(Weights_field))
  else 
    st_sfc(mean_mc(), median_mc(), central_feature())
}

mc <- switch(Spatial_center,
             mean.center = mean_mc(),
             weighted.mean.center = mean_mc(Weights_field),
             median.center = median_mc(),
             central.feature = central_feature(),
             all = all_features()
)

Centrality <- st_as_sf(st_geometry(mc), crs = st_crs(Layer))
Centrality$Name <- c("Mean center", "Median center", "Central feature", "Weighted mean center")[seq_len(nrow(Centrality))]
