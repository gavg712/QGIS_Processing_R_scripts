##Raster processing=group
##Extract raster values by point=name
##Raster=multiple raster
##Points=vector point
##Output_column_prefix=optional string
##Extracted=output vector
Raster <- brick(Raster)
pntcrs <- st_crs(Points)
samecrs <- st_crs(Raster) == st_crs(Points)
if(!samecrs){
  warning("Raster and Point are from different CRS")
  Points <- sf::st_transform(Points, sf::st_crs(Raster))
}

Extracted <- extract(Raster, Points, sp = TRUE)

if(!is.null(Output_column_prefix))
  names(Extracted) <- paste0(Output_column_prefix,  names(Extracted))

Extracted <- st_as_sf(Extracted)
if(!samecrs) Extracted <- sf::st_transform(Extracted, pntcrs)