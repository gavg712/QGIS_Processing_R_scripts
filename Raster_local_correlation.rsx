##Raster processing=group
##Local correlation coefficient=name
##Raster_x=raster
##Raster_y=raster
##Neighborhood_size=number 5 
##Method=enum literal pearson;kendall;spearman pearson
##P_value=boolean false
##Local_correlation=output raster

Local_correlation <- raster::corLocal(
    x = Raster_x[[1]],
    y = Raster_y[[1]], 
    ngb = Neighborhood_size,
    method = Method, 
    test = P_value
)
